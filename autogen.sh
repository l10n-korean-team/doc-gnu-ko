#!/bin/sh
# Run this to generate all the initial makefiles, etc.

AM_STRICTNESS="--foreign"

set -e

echo "** Processing..."

grep -l "AM_GNU_GETTEXT" configure.in > /dev/null && {
    echo "** It uses gettext, running gettextize..."
    echo "no" | gettextize --force --copy > /dev/null
}

grep -l "AM_PROG_LIBTOOL" configure.in > /dev/null && {
    echo "** It uses libtool, running libtoolize..."
    libtoolize --force --copy --automake > /dev/null
}

echo "** Running aclocal..."
aclocal $ACLOCAL_FLAGS

grep -l "AC_CONFIG_HEADER" configure.in > /dev/null && {
    echo "** It uses config header, running autoheader..."
    autoheader
}

# Just to avoid confusions.  Automake doesn't put ./configure into
# $(DISTFILES) if it's not found.
touch configure

echo "** Running automake..."
automake $STRICTNESS --add-missing

echo "** Running autoconf..."
autoconf

echo "** Running ./configure..."
./configure --enable-maintainer-mode "$@"

echo "** Get ready.  Happy hacking."
