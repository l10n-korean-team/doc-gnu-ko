\input texinfo-ko    @c -*-texinfo-*-
@comment %**start of header 
@setfilename info-ko.info
@settitle Info 1.0
@comment %**end of header 

@dircategory Texinfo documentation system
@direntry
* Info: (info-ko).              문서 보기 시스템
@end direntry

@iftex
@finalout
@end iftex

@ifinfo
This file describes how to use Info, 
the on-line, menu-driven GNU documentation system.

Copyright (C) 1989, 92, 96 Free Software Foundation, Inc.

Permission is granted to make and distribute verbatim copies of
this manual provided the copyright notice and this permission notice
are preserved on all copies.

@ignore
Permission is granted to process this file through TeX and print the
results, provided the printed document carries copying permission
notice identical to this one except for the removal of this paragraph
(this paragraph not being relevant to the printed manual).

@end ignore
Permission is granted to copy and distribute modified versions of this
manual under the conditions for verbatim copying, provided that the entire
resulting derived work is distributed under the terms of a permission
notice identical to this one.

Permission is granted to copy and distribute translations of this manual
into another language, under the above conditions for modified versions,
except that this permission notice may be stated in a translation approved
by the Free Software Foundation.
@end ifinfo

@setchapternewpage odd
@titlepage
@sp 11
@center @titlefont{Info}
@sp 2
@center The
@sp 2
@center On-line, Menu-driven
@sp 2
@center GNU Documentation System

@page
@vskip 0pt plus 1filll
Copyright @copyright{} 1989, 1992, 1993 Free Software Foundation, Inc.
@sp 2

Published by the Free Software Foundation @*
59 Temple Place - Suite 330 @*
Boston, MA 02111-1307, USA.

Permission is granted to make and distribute verbatim copies of
this manual provided the copyright notice and this permission notice
are preserved on all copies.

Permission is granted to copy and distribute modified versions of this
manual under the conditions for verbatim copying, provided that the entire
resulting derived work is distributed under the terms of a permission
notice identical to this one.

Permission is granted to copy and distribute translations of this manual
into another language, under the above conditions for modified versions,
except that this permission notice may be stated in a translation approved
by the Free Software Foundation.
@end titlepage


@paragraphindent 3
@ifinfo
@node Top, 시작하기, (dir), (dir)
@top Info: 소개

Info는 문서 보기 풀그림인데, 지금 보고 있는 것이다.

Info 사용법을 배우려면, @kbd{h} 글쇠를 누른다. 
이것은 Info 사용법을 배우는데, 체계적인 과정을 진행한다. 
언제든지 이 Info 보기를 그만두고자 하면, @samp{q} 글쇠를 누른다.

@c Need to make sure that `Info-help' goes to the right node, 
@c which is the first node of the first chapter. (It should.) 
@c   (Info-find-node "info"
@c 		  (if (< (window-height) 23)
@c 		      "작은 화면 도움말"
@c 		    "도움말")))

고급 Info 명령을 배우려면, @kbd{n} 글쇠를 두번 누른다. 
이것은 `시작하기' 노드를 건너뛰고, @cite{전문가를 위한 Info} 
노드로 이동한다.
@end ifinfo

@menu
* 시작하기::                info를 처음 사용하는 사람들 위해
* 고급 Info::               고급 사용자를 위한 info 명령
* Info 파일 만들기::        info 파일을 만드는 방법
@end menu

@node 시작하기, 고급 Info, Top, Top
@comment  node-name,  next,  previous,  up
@chapter 시작하기

Info 사용 설명서의 첫부분은 info를 처음 사용하는 사람들을 위한 
그 사용법에 대해서 다루고 있다. 다음 둘째부분에서는 다양한 
고급명들에 대해서, 셋째부분에서는 Texinfo 파일에서 info 파일을 
만들어내는 방법에 대해서 다루고 있다. 

@iftex
This manual is primarily designed for use on a computer, so that you can
try Info commands while reading about them.  Reading it on paper is less
effective, since you must take it on faith that the commands described
really do what the manual says.  By all means go through this manual now
that you have it; but please try going through the on-line version as
well.  

There are two ways of looking at the online version of this manual:

@enumerate
@item
Type @code{info} at your shell's command line.  This approach uses a
stand-alone program designed just to read Info files.

@item
Type @code{emacs} at the command line; then type @kbd{C-h i} (Control
@kbd{h}, followed by @kbd{i}).  This approach uses the Info mode of the
Emacs program, an editor with many other capabilities.
@end enumerate

In either case, then type @kbd{mInfo} (just the letters), followed by
@key{RET}---the ``Return'' or ``Enter'' key.  At this point, you should
be ready to follow the instructions in this manual as you read them on
the screen.
@c FIXME! (pesch@cygnus.com, 14 dec 1992)
@c Is it worth worrying about what-if the beginner goes to somebody
@c else's Emacs session, which already has an Info running in the middle
@c of something---in which case these simple instructions won't work?
@end iftex

@menu
* 작은 화면 도움말::     작은 화면에서 Info 시작하기
* Help::                 Info 사용법
* 도움말-P::             이전 노드로 돌아가기
* 도움말-^L::            사이띄우게, Delete, B, ^L 명령.
* 도움말-M::             차림표
* 도움말-고급::          Info의 약간의 고급 명령 도움말 
* 도움말-Q::             Info 마치기
* info 풀그림 사용하기:: emacs 사용하지 않고 단독으로 info 사용하기
@end menu

@node 작은 화면 도움말, Help,  , 시작하기
@comment  node-name,  next,  previous,  up
@section 작은 화면에서 Info 시작하기

@iftex
(In Info, you only see this section if your terminal has a small
number of lines; most readers pass by it without seeing it.)
@end iftex

Info의 화면을 보면 일반 info 내용이 나타나는 부분과 
아랫부분에 역상으로 여러가지 정보들이 나타나는 상황선이 있다.
(이 글에서는 이 상황선을 @samp{작은 화면(small screen)}이라
부른다.) info의 사용법을 설명하기 이전에 먼저 이 작은 화면에 
대한 이야기를 할 필요가 있다.

이 화면의 가운데에서 오른쪽에 @samp{--All----} 이런 글자가 
보인다면, 이것은 현재 화면에서 현재 노드의 내용을 보여 주고 있다는 
이야기다. 대신에 @samp{--Top----} 이런 글자가 보인다면, 
이것은 현재 노드의 제일 처음에 있고 이 화면 다음에도 내용이 
더 있다는 이야기다. 이때 다음 내용을 보기 위해서는 
화면 이동 명령을 사용하는데, 먼저 한 화면씩 움직이는 명령으로 
다음 화면을 보려면 @key{사이띄우게}를 이전 화면을 보려면 
@samp{Delete}라고 쓰여진 글쇠나 @key{지움}글쇠를 사용한다.

@ifinfo
다음부터는 특별한 내용이 없는 40줄이 나타난다. 이것은 
사이띄우게와 Delete글쇠가 어떤 일을 하는지 살펴 보기 위해서다.
이 노드의 끝부분에서 다음에 무엇을 해야할 것인지 이야기하고 있다.
@format
This is line 17
This is line 18
This is line 19
This is line 20
This is line 21
This is line 22
This is line 23
This is line 24
This is line 25
This is line 26
This is line 27
This is line 28
This is line 29
This is line 30
This is line 31
This is line 32
This is line 33
This is line 34
This is line 35
This is line 36
This is line 37
This is line 38
This is line 39
This is line 40
This is line 41
This is line 42
This is line 43
This is line 44
This is line 45
This is line 46
This is line 47
This is line 48
This is line 49
This is line 50
This is line 51
This is line 52
This is line 53
This is line 54
This is line 55
This is line 56
@end format
지금 이 글을 보고 있다면, Delete 글쇠를 사용해서 이 노드의 
처음으로 돌아가서 몇번 더 연습해 보기 바란다. 
이제, @kbd{n} 글쇠를 쳐보자. 이때 작은 따움표도 칠 필요없고 
n 다음 @kbd{Enter}글쇠도 칠 필요 없다.
@end ifinfo

@node Help, 도움말-P, 작은 화면 도움말, 시작하기
@comment  node-name,  next,  previous,  up
@section Info 사용법

당신은 지금 Info라는 풀그림을 보고 있는데, 이것은 문서를 
읽는 것이다.

  그래서 지금 당신은 @dfn{노드}라는 것에 대해서 보고 있다. 
하나의 노드는 특정한 내용과 그에 해당하는 제목으로 구성된다.
지금 보고 있는 이 노드의 제목은 ``Info 사용법''이다.

  각 노드의 첫줄에는 @dfn{머릿말}이 있는데, 
이 노드의 머릿말은 현재 @file{info}라는 파일 안에 있는 
@samp{Help}이라는 이름의 노드를 알리고 있다.
다음에는 @samp{도움말-P}라는 것이 다음 노드의 이름임을 알리고 있다.
고급 Info 명령으로 당신이 알고 있는 노드의 이름으로 바로 이동할 
수도 있다. 이때 이 노드 이름들이 사용된다.

  @samp{Next} 다음에는 @samp{Previous}, @samp{Up} 글자들이 있는데, 
이것은 각각 이전 노드 이름이 @samp{작은 화면 도움말}이고, 
상위 노드 이름은 @samp{시작하기}임을 알린다.
어떤 노드에서는 이 @samp{Previous}나 @samp{Up}이 없는 경우도 있다.

  이제 @samp{Next}에서 알리고 있는 @samp{도움말-P}이라는 이름을 
가진 노드로 이동해 해보자.

@format
>> @samp{n} 글쇠를 쳐보자. 단지 한 글쇠만.
   작은 따움표를 칠 필요도 없고, n 글쇠 다음 @key{Enter}도 칠 필요 없다.
@end format

@samp{>>} 기호로 시작하는 내용은 직접 해보라는 뜻이다.

@node 도움말-P, 도움말-^L, Help, 시작하기
@comment  node-name,  next,  previous,  up
@section 이전 노드로 돌아가기

이 노드의 이름은 @samp{도움말-P}이다. @samp{Previous} 노드는, 보듯이, 
@samp{Help}이고, 이것은 좀전에 @kbd{n} 명령을 내렸던 노드이다. 
이 노드에서 다시 @kbd{n} 명령을 내리면 다음 노드인 @samp{도움말-^L}로
이동하게 된다.

@format
>> 하지만 지금 실행보지 말고, 먼저, @kbd{p} 명령으로  @samp{Previous}
   노드로 이동한 후, 그곳에서 @kbd{n} 명령으로 다시 이곳에 오십시오.
@end format

  이것은 꼭 장난 치는 것 같지만, 쓸모 없는 작업은 아니다. 
보다 복잡한 명령은 좀 있다가 배우게 될 것이다. 그러니,
새 명령들에 대해서 벌써부터 연습해 볼 필요는 없다. 

@format
>> 이제 @kbd{n} 글쇠를 쳐서 @samp{도움말-^L} 노드로 가보자.
@end format

@node 도움말-^L, 도움말-M, 도움말-P, 시작하기
@comment  node-name,  next,  previous,  up
@section 사이띄우게, Delete, B, ^L 명령.

  화면을 살펴보자. 먼저, 화면 첫줄, 머릿말 부분에서는 현재 노드가 
@samp{도움말-^L} 임을 알리고, 다음 @kbd{p} 글쇠를 치면, 좀전에 보았던,
@samp{도움말-P} 노드로 돌아감을 알린다. 그리고, 그 아랫줄에는 
밑줄이 그어진 이 노드의 제목이 보인다. 이 제목은 현재 이 노드에서 
어떤 이야기를 하고 있는가를 알린다. 

  화면 크기를 임의로 변경하지 않았다면, 화면의 아랫쪽 역상부분으로
보여지는 줄에서 @samp{--Top-----}이라고 나올 것이다. 이것은 앞에서 
설명했듯이 이 노드의 내용은 한 화면에 다 보일 수가 없기에, 현재 
위치는 이 노드의 처음 부분임을 알린다. 만일 @samp{--All----}이라고 
나온다면, 화면에 보여지는 것 외에는 다른 내용이 없음을 알린다.

  사이띄우게와 Delete, @kbd{B} 글쇠를 치면, 각각 화면의 내용을 
한 화면씩(화면 전체적으로) 바꾼다. 사이띄우게는 한 화면 다음(아래)
내용을 보여주며, Delete 글쇠는 한 화면 이전(윗) 내용을 보여주며,
@kbd{B} 글쇠는 현재 노드의 처음으로 이동한다. 여기서 재미있는 것은 
사이띄우게와 Delete 글쇠는 계속 누르고 있으면, 계속 그 해당 작업을 
한다는 것이다. 즉, 현재 노드의 끝으로 왔을 때 사이띄우게를 누르면, 
현재 노드에서 지정한 다음 노드로 이동해서, 그 노드의 내용을 보여주게 
된다. 결국 @kbd{n} 명령과 같은 역활을 하게 된다.

@format
>> 그럼 사이띄우게를 한번 쳐서 다음 내용으로 가서 다시 Delete 글쇠를 
   쳐서 이곳으로 돌아와 보자.
@end format

  사이띄우게와 Delete 글쇠를 누르때, 화면의 내용을 유심히 살펴보면, 
각각 아래 위 두 줄을 남겨두고 스크롤 되는 것을 알 수 있다. 

  info를 사용하다 보면, 터미날 환경에 따라 지나간 글을 제대로 
지우지 못해 화면이 지저분해 질 경우가 있다. 이때 현재 화면을 
다시 깔끔하게 보여주는 기능을 하는 명령이 바로 @kbd{C-l} 이다.
(@kbd{Control-L}이라는 뜻으로, ``Ctrl'' 글쇠를 누르고 있으면서 
@key{L}이나, @kbd{l} 글쇠를 치는 것을 의미한다.)

@format
>> 지금 @kbd{C-l} 해보자.
@end format

  현재 노드의 처음으로 가고 싶은데, Delete 글쇠를 여러번 눌러야 할 경우라면,
이때 @kbd{b} 글쇠를 치면 바로 현재 노드의 처음으로 이동한다.

@format
>> 지금 확인해 보자. (그저 간단히 Delete 글쇠 몇번 누르면 될 문제를 
   왜 복잡하게 이렇게 했냐하면, 요즘은 화면이 커서 별 차이가 없겠지만, 
   Emacs에서 Info 창을 열어서 사용할 때는 이 명령이 아직도 유용하게 
   쓰인다.) 다시 이곳으로 오려면 어떻게 할까?
@end format

화면이 엄청나게 커서 현재 노드의 내용을 한 화면에 다 볼 수 있다면, 
당연히 "b" 명령은 아무런 반응을 보이지 않는다. (근데, 이 노드를 
다보여주는 화면도 있으려나^^;)

  이제 꽤 많은 명령을 배웠다. 어떤 기능이 있는지는 아는데, 
그 명령이 뭔지 잊어버렸을 경우에, @key{?} 글쇠를 치면, 
info에서 사용할 수 있는 모든 명령들을 보여준다. 이 도움말을 
다 보았을 경우에는 그냥 사이띄우게를 계속 누르면 된다. 

@format
>> @key{?} 글쇠를 눌러 도움말을 끝까지 살펴보고, @key{SPC} 글쇠를 눌러보자.
@end format

  (지금 emacs의 info 보기가 아닌, info 풀그림을 보고 있다면, 
   `l' 명령을 사용해서 이곳에 올 수도 있다.)

  지금까지 명령을 익혔다면, 일단 info 파일을 읽는 데는 별 지장이 
없을 것이다. 마치 책을 읽듯, 차근하게 차례로 읽을 나갈 수 있을 
것이다. 이제 다음 몇개의 글만 더 읽는다면, info 파일의 내용을 
마음대로 옮겨가며 읽을 수 있을 것이다.

@format
>> 이제 @kbd{n}을 쳐서 @kbd{m} 명령에 대해서 살펴보자.
@end format

@node 도움말-M, 도움말-고급, 도움말-^L, 시작하기
@comment  node-name,  next,  previous,  up
@section 차림표

차림표와 @kbd{m} 명령

  @kbd{n}과 @kbd{p} 명령으로는 지정된 노드 사이를 오가며 글을 읽을 수
밖에 없다. 차림표(``Menu''라고 적힌 부분의 아랫부분)는 마치 책의 글 차례처럼
각 노드의 이름과 간단한 설명이 나열되어 있고, 그 노드로 바로 이동한다. 
차림표는 항상 @samp{* Menu:} 
글자로 시작한다. info의 특징은 바로 이 차림표 기능과 상호참조(cross
reference)기능을 제공하는 하이퍼텍스트 시스템이라는 것이다.

  차림표 항목은 한줄씩으로 되어있는데, 그 첫칸에는 @samp{*} 표가 
있다. 이 표시 다음에는 그 항목의 이름이 나오고, @samp{:} 표시로 
그 항목의 이름이 끝남을 알리고, 그 다음에는 그 항목의 간단한 
설명이 있다. 차림표 항목 중에 첫칸에 @samp{*} 표시가 없는 경우도 
있는데, 특정 노드가 지정되어 있는 것이 아니라, 단지 그 글 자체,
읽는 것만 할 수 있다. 일반적으로 차림표는 다음과 같이 나타난다.
(아래 것은 진짜 차림표가 아니라, 차림표 예제이다.)

@example
* 예제:  예제 노드.     이것은 차림표 예제이다
@end example

이 차림표 항목은 한개이고, 그 항목의 이름은 `예제'이며, 
그 항목의 노드이름은 @samp{예제 노드}이다. 나머지 부분은 
읽는 사람이 알기 쉽도록 설명해 둔 것이다. 
[[즉, 엄밀히 말하면, ``이것은 차림표 예제이다''라는 내용은 
차림표 안에 포함되지 않는다. 그래서, 이 내용의 길이가 길 경우, 
다음 줄에 나타나는데, 이때는 그 줄 첫칸에 @samp{* } 표가 
나타나지 않는다.]]

  이제 이 차림표의 항목으로 이동하고 한다면, 바로 이 항목의
이름을 사용한다. info에서는 바로 이 이름으로 각 노드를 찾고 
그 노드로 이동하게 되는데, 이런 이유로 이 이름들은 사람이 알기 
쉽도록 작성되기 보다는 컴퓨터가 알기 쉽도록 작성되는 경우가 
대부분이다. 그래서, 각 항목에 대한 간단한 설명이 있는 경우가 
대부분이다. 한편, 노드의 이름과 차림표 항목의 이름이 같은 경우가
있는데, 이것은 다음과 같은 형식으로 나타난다.

@example
* 예제::   이것은 차림표 예제이다.
@end example

@noindent
윗 표시는 노드의 이름과 차림표 이름이 모두 @samp{예제}라고 알리고 있다.

@format
>> 이제, 사이띄우게를 사용해서, 이 노드에서 차림표를 찾아보자. 
   사이띄우게를 몇번 누르면 차림표가 보일 것이다. 
   만일 발견하지 못했다면, @kbd{m}을 쳐보자. 이때, 이 명령을 
   사용할 수 없다면, 이 노드에는 차림표가 없는 것이다.
@end format

  차림표의 각 항목으로 이동하는 명령은 @kbd{m}이다 -- 하지만
@emph{지금은 사용하지 말기를!} @kbd{m} 명령을 사용하기 전에 먼저, 
명령과 그 명령의 인자에 대해서 이해할 필요가 있다. 지금까지 배운 
명령들에서는 명령의 인자가 필요없었다. 즉, 그냥 그 명령을 내리기만
하면, info에서는 바로 그 명령에 따른 작업을 했었다. 하지만, 
@kbd{m} 명령은 다르다. 이것은 어떤 항목으로 이동할 것인지를 물어본다.

  이제 화면의 아랫쪽을 보자. 그곳에 보면, 역상으로 나타는 줄이 
하나 있고 그 밑에 빈줄로 또 한 줄이 있다. 바로 그 줄에서 명령의 인자를
입력받는다. 즉, @kbd{m} 명령과 같은 명령의 인자가 필요한 명령이 
사용되면, 커서가 그 화면 마지막 줄로 옮겨지면서, 사용자의 입력을 
기다린다. 이때, 사용자는 적당한 인자값을 입력해야 한다. 입력의 끝은 
엔터로 끝난다. 이때, 현재 입력 작업을 취소하고자 한다면,  @kbd{Control-g}를
누른다. 

  @kbd{m} 명령에서는 @kbd{m}을 치면, 화면 마지막 줄에, @samp{Menu item: }
글자가 나타나고, 그 다음에 커서가 나타나, 당신의 입력을 기다린다. 
이때, 원하는 항목의 이름을 입력해주고, 엔터를 누르면 된다. 

  이때, 그 항목의 이름을 모두 적어줄 필요는 없다. 앞의 몇자만 적어주고, 
그것이 다른 항목의 이름들과 구별지을 수 있다면, 그 구분된 노드로 
이동한다. 영문일 경우, 대소문자 구분을 하지 않으며, 적어주는 이름의 
끝에 공백문자는 허용하지 않는다. 

  또한 쉘 명령행 입력할 때 처럼 Tab 글쇠를 이용해 나머지 부분을 
자동으로 채워주는 기능을 이용할 수도 있다. (쉘에서의 기능과 똑같다.)

  만일, 현재 커서가 차림표 항목에 와 있다면, @kbd{m} 명령은 인자를 
초기값으로 현재 커서가 있는 항목의 이름을 사용한다. 그래서, 
만일 바로 그 항목으로 이동하고자 한다면, 그냥 엔터를 누르면 된다.

여기 이제 지금까지 배운 것을 연습해 볼 차림표가 있다.

@menu
이 차림표는 도움말-예제라는 노드로 이동하는 세가지 항목으로 구성되어있다.

* 예제:  도움말-예제.       장난 삼아 한번 가보자.
* 연습:  도움말-예제.       이런 방법으로 갈 수 있다.
* 도움말-예제::             이정도면 충분할까나?
@end menu

@format
>>  이제 @kbd{m} 글쇠를 쳐서 어떤 일이 벌어지는지 보자.
@end format

  이러면, 커서가 화면 마지막 줄로 현재 옮겨져 있을 것이다. 
아직 다른 작업을 하지 말고, 이 글을 마자 읽기 바란다. 이제 
차림표 항목의 이름을 입력해 보자.

  만일 굳이 해보고 싶지 않다면, Ctrl-g 명령으로 중지하면 된다.

@format
>> Ctrl-g를 눌러 어떻게 되는지 살펴 보자.

>> 이제 다시 @kbd{m}을 눌러

>> @samp{예제}라고 입력하고, 엔터는 치지말고.
@end format

  입력 도중에 입력할 글자들들 수정하려면, Delete 글쇠를 이용한다.

@format
>> 이제 준비가 되었으면, 엔터를 친다.
@end format

  이 자습서를 계속 읽고 싶으면, 도움말-예제로 갔다가 다시 이곳으로 
돌아와야하는 것을 잊지말도록.

@format
>> @kbd{n} 명령을 이용해 다음 명령들을 살펴 보자.
@end format

@c If a menu appears at the end of this node, remove it.
@c It is an accident of the menu updating command.

@node 도움말-예제,  ,  , 도움말-M
@comment  node-name,  next,  previous,  up
@subsection @kbd{u} 명령

  축하! 이 노드의 이름은 @samp{도움말-예제}이다. 이 노드는 다른 노드와 
달리, 이전, 다음 노드가 지정되어 있지 않았다. 즉, n, p 명령으로는 
이 노드로 올 수 없으며, @kbd{m} 명령으로 올 수 있다. 이제 상위 노드로 
이동하는 @kbd{u} 명령을 배워야 할 시기다. 차림표는 현재 노드를 바탕으로
그 하위 노드로 이동하는 방식을 취한다. 하지만, n, p 명령으로 
이동하는 노드는 그 계층구조가 같은 서열에 있는 노드들이다. @kbd{m} 명령으로
하위 노드로 이동했을 경우, 다시 상위 노드로 돌아가고자 한다면, 
이전 노드로 이동하는 p 명령이 아니라, u(Up) 명령을 사용한다. 

  즉, 조금 전에 보았던 @samp{도움말-M} 노드로 돌아가려면, @kbd{u} 글쇠를
눌러야한다. 이 명령은 그 노드의 처음으로 이동한다. 그래서, 
조금 전에 읽었던 부분으로 돌아가려면, 다시 사이띄우게를 사용해야한다. 

@format
>> @samp{도움말-M} 노드로 돌아가기 위해 @kbd{u}를 치자.
@end format

@node 도움말-고급, 도움말-Q, 도움말-M, 시작하기
@comment  node-name,  next,  previous,  up
@section Info의 약각의 고급 명령 도움말

  이 자습서의 끝부분이다. 끝까지 읽고 숙지하시길.

  만약에 여러 노드를 이동하다가 다시 바로 이전의 상태로 되돌리고 
싶을 경우에는 @kbd{l} 명령을 사용한다. @kbd{l}은 @dfn{last}를 의미한다.
이 명령은 지금껏 배운 이동 명령들과 달리 그 이동 명령들에 대한 명령이다.
당신이 한 노드에서 다른 노드로 이동할 때, info에서는 어디서 그 노드로 
이동했는지에 대한 정보를 기록한다. @kbd{l} 명령은 바로 그 기록 정보를 
거꾸로 되돌리는 역활을 한다.

  현재 이 자습서의 내용에 따라 이 글을 읽고 있다면, 첫번째 @kbd{l} 명령은
@samp{도움말-M} 노드로, 그다음 @kbd{l} 명령은 @samp{도움말-예제} 노드로
이동할 것이다. 또 그다음 @kbd{l} 명령은 @samp{도움말-M} 노드로 이동할 
것이다.

@format
>> @kbd{l} 글쇠를 세번 쳐서 화면이 어떻게 바뀌는지 확인해 보자.
@end format

다음 다시 이곳으로 돌아오기 바란다.

  @kbd{l} 명령과 @kbd{p} 명령의 차이점은, @kbd{l} 명령은 @emph{당신이}
옮겨다닌 노드를 대상으로 하고 있고, @kbd{p} 명령은 항상 @samp{Previous}
에서 지정된 노드를 대상으로 하고 있다. 

  @samp{d} 명령은 info에서 살펴 볼 수 있는 최상위 노드이다. 
즉, info를 아무 옵션이나, 인자 없이 시작하게 되면 보여주는 것이 
바로 이 노드이다. 이것은 dir 파일의 내용이며, @samp{d} 명령은 
바로 이 dir 파일의 내용을 읽는다.

@format
>> @samp{d}를 쳐서, 구경 한번 하고, 다시 @kbd{l}을 쳐서 이리로.
@end format

  가끔 info 문서 파일안에서 상호참조(cross reference)를 볼 수 있는데,
다음과 같이 나타난다 @xref{도움말-상호참조, 상호참조}. 
여기서 "* NOTE:" @samp{상호참조}라고 표현된 것이 바로 상호참조이며, 
이것은 @samp{도움말-상호참조} 노드를 가리키고 있다.

  이 상호참조로 이동하려면, @samp{f} 명령을 사용한다. @samp{f} 명령의 
인자로 그 참조할 이름을 입력하는데, 여기서는 @samp{이런식}이 된다.
여느 인자 입력방식과 같이 Delete 글쇠로 입력을 수정할 수 있으며, 
@kbd{Control-g}로 입력을 취소할 수 있고, Tab으로 나머지 부분을 
자동으로 채울 수도 있다. 

@format
>> @samp{f}를 누르고, @samp{상호참조}를 입력하고, 엔터를 쳐보자.
@end format

  현재 노드에서 참조할 수 있는 모든 상호참조를 보려면, @samp{f}를 
친 다음, @kbd{?} 글쇠를 누른다. 그러면 그 목록들이 모두 나열된다.

@format
>> "f?" 글쇠를 쳐서 상호참조 내용들을 살펴보고, @kbd{Control-g}를 
   쳐서 취소를 해보자.

>> 이제 @kbd{n}으로 이 자습서의 마지막으로 가자.
@end format

@c If a menu appears at the end of this node, remove it.
@c It is an accident of the menu updating command.

@node 도움말-상호참조,  ,  , 도움말-고급
@comment  node-name,  next,  previous,  up
@unnumberedsubsec 이 노드는 info에서 상호참조로 나타난다

  이 노드는 @samp{상호참조}이라는 이름을 가진 상호참조에 의해서 
불려지는 노드이다.

  이런 상호참조 노드의 경우는 대게 전체적인 info 파일의 구조와 
관계없이 특정 노드에 의해서만 관계되어 나타난다. 그래서, 
@samp{Next}, @samp{Previous} 또는 @samp{Up} 노드에 대한 
지정이 없을 수도 있다. 이런 경우에는 @kbd{l} 명령으로 이전 노드로 
돌아간다.

@format
>> @kbd{l}을 쳐서 이전으로 돌아가자.
@end format

@node 도움말-Q,  , 도움말-고급, 시작하기
@comment  node-name,  next,  previous,  up
@section Info 마치기.

  Info를 끝내려면, @kbd{q} 글쇠를 누른다.

  이것이 이 자습서의 마지막이다. 물론 이 파일에는 여러 다른 
명령들을 설명하고 있지만, 이것은 이미 info를 사용할 줄 아는 사람을 
위한 것이다. 이제부터 이런 명령들을 익혀 보다 유용하게 info를 사용하기를
바란다. 아마 이 자습서를 통해서 info란 어떤 것이고, 어떻게 움직이는가
정도는 알 수 있었을 것이다.

@format
>> 이제 @samp{d}를 쳐서, 최상위 노드로 돌아간 다음, @samp{mInfo}를 
   쳐서, 아직 보지 않았던 부분들에 대해서 살펴 보기 바란다. 
@end format

@node 고급 Info, Info 파일 만들기, 시작하기, Top
@comment  node-name,  next,  previous,  up
@chapter 전문가를 위한 Info

여기서는 info의 여러가지 고급 명령들에 대해서 다루고 있으며, 
Texinfo 파일을 이용하는 것과 구분해서 그냥 info 파일을 작성하는 방법을 
설명하고 있다.(하지만, 대부분의 경우, info 파일은 Texinfo 파일로 
만들어진다. 왜냐하면, Texinfo 파일은 온라인 매뉴얼인 info 파일과 
일반 인쇄물로 만들 수 있는 ps 파일을 모두 만들어 낼 수 있기 때문이다.
@xref{Top,, Overview of Texinfo, texinfo, Texinfo: The GNU Documentation Format}.)

@menu
* 전문가::                고급 Info 명령들: g, s, e, 1 - 5.
* 추가::                  새로운 노드 만드는 방법
* 차림표::                Info 노드에서 차림표 추가 방법.
* 상호참조::              Info 노드에 상호참조 추가 방법.
* 태그::                  tags table 만드는 방법.
* 검사하기::              Info 파일 검사하기.
* Emacs Info 변수들::     Emacs Info의 작업 환경을 바꾸는 변수들.
@end menu

@node 전문가, 추가,  , 고급 Info
@comment  node-name,  next,  previous,  up
@section 고급 Info 명령들

@kbd{g}, @kbd{s}, @kbd{1}, -- @kbd{9}, @kbd{e}

이미 어떤 노드의 이름을 알고 있다면, @kbd{g} 글쇠를 누른다면, 
그 노드 이름을 입력하고, 엔터를 치면, 해당 노드로 바로 이동할 수 있다.
여기서 @kbd{gTop@key{RET}} 이렇게 입력된다면, info의 Top 노드로 
이동할 것이고, 그곳에서 @kbd{g전문가@key{RET}} 이렇게 하면, 
다시 이곳으로 올 것이다.

@kbd{m} 명령과 달리, Tab 글쇠를 사용하는 나머지 채움 기능을 사용
할 수 없다.

다른 파일의 노드로 이동하려면, 먼저 괄호()로 그 파일을 묶어 
노드 이름 앞에 써 주어야한다. 그래서, @kbd{g(dir)Top@key{RET}} 
이렇게 하면, info의 최상위 노드인 dir 파일의 Top 노드로 이동하게
된다. 현재 여기서 그냥 @kbd{gTop@key{RET}} 명령을 내리면, 
이 info에 대한 info 파일의 Top 노드로 이동한다(차이점). 
한편 파일이름만이 괄호()로 묶겨 지정되면, 그 파일의 Top를 찾고, 
있다면, 보여주고, 없다면, 없다고, 오류 메시지를 알려준다.

@samp{*} 명령은 그 파일의 모든 내용을 보여준다. 즉, 현재 info로 
볼 수 없는 여러가지 정보들도 함께 볼 수 있다. 
여기서 @kbd{g*@key{RET}} 이렇게 명령을 내린다면, 
이 파일의 저작권 이야기를 시작으로 하는 이 파일의 모든 것을 볼 수
있다. 또한 현재 파일이 아닌 다른 파일의 모든 내용을 보려면, 
@kbd{g(파일이름)*@key{RET}} 식이 된다.

@kbd{s} 명령은 현재 파일 안에서 지정한 문자를 찾는다. 필요하다면, 
다음 노드로 전환된다. 사용방법은 @kbd{s} 글쇠를 누르고, 
찾고자 하는 문자열을 입력하고, @key{RET} 글쇠를 누른다. 
계속 같은 것을 찾고자 한다면, @kbd{s} 글쇠를 누르고, 
그냥 @key{RET} 글쇠를 누른다. 
이 문자열 검색은 노드의 관계 구조를 검색을 한다. 
즉, 원래의 파일의 내용이 기록된 순서대로가 아니다. 이 두개의 순서는 
사뭇 다를 수가 있다. 즉, 이 명령을 사용하기 위해서는 항상 
@kbd{b} 명령으로 노드의 처음으로 이동한 뒤에 이 명령을 사용하는 것이 
좋다. 왜냐하면, 현재 위치의 이전에 찾고자 하는 문자열이 있고, 
지금 커서가 있는 위치부터 이 노드의 아래쪽에는 찾고자 하는 문자열이 
없을 경우 @kbd{s} 명령은 다음 노드로 이동해서 그 문자열을 찾기 
때문이다. (결국 찾고자 하는 문자열이 여럿 있고, 현재 노드의 
커서 위쪽에 있는 부분에서 그 문자열을 찾고자 했는데, 현재 커서 위치에서
@kbd{s} 명령을 사용해 버리면 그 여러개 있는 문자열 중에 가장 마지막에 
발견된다.)

@kbd{Meta-s} 명령은 @kbd{s} 명령과 같다. 이것은 여러 다른 
GNU 풀그림들이 @kbd{M-s} 글쇠로 찾기 명령을 제공하기 때문에, 
지정되었다.

글쇠 치기를 귀찮아 하는 사람들을 위해서 여기 유용하게 쓰일 
명령이 있다. @kbd{1}, @kbd{2}, @kbd{3}, @kbd{4}, .... 
이 숫자 글쇠는 현재 노드에 있는 첫번째, 두번째, 세번째, ....
차림표로 바로 이동하게 기능을 한다. 

게다가 다중 글꼴을 지원하는 emacs에서는, 다섯번째, 아옵번째
차림표를 표시하는 @samp{*} 글자에 밑줄 속성이 부여되어 있다. 
즉, 숫자 글쇠 사용을 쉽게 하기 위해.

그냥 보통의 터미날에서는 밑줄이 보여지지 않을 것이다. 
읽고 싶은 차림표가 몇번째 있는지 헤기 보다, 그냥 @kbd{m} 글쇠를 
누르고 직접 차림표의 이름을 입력하기는 것이 좋을 것이다.

Emacs Info에서는 @kbd{e} 명령으로 읽기 전용 상태에서 
편집 가능한 상태로 전환할 수 있다. 편집이 끝나면, 
@kbd{C-c C-c} 명령으로 다시 읽기 전용 상태로 돌아온다. 
이 명령은 @code{Info-enable-edit} 변수값이 @code{nil}이 
아닐 때만 사용할 수 있다.

@node 추가, 차림표, 전문가, 고급 Info
@comment  node-name,  next,  previous,  up
@section 새 노드 추가하는 방법

dir 파일 안에 새로운 항목을 추가하는 방법은 비교적 간단하다.
먼저 추가할 항목의 파일 이름과, 그 노드 이름만 알고 있으면
차림표 항목을 추가하는 방법을 대로 사용하면 된다. @xref{차림표}.

하지만, 하나의 새로운 노드를 만들고, 그 노드들을 종합해서, 
또다른 하나의 info 파일을 만든다는 것은 그리 간단한 작업이 
아니다. 이런 번거러운 작업을 쉽게 하도록 도와주는 것이
GNU 문서의 형식화 방식의 표준인 Texinfo이다. 
@pxref{Top,, Overview of Texinfo, texinfo, Texinfo: 
The GNU Documentation Format}); texinfo 포멧 방식을 사용하면, 
양질의 인쇄물도 같이 얻을 수 있다는 장점이 있다. 
하지만, 단지 info 파일을 texinfo를 이용하지 않고 만들겠다면, 
다음 이야기를 참조한다.

  새로운 노드는 이미 있는 info 파일에 추가 할 수도 있으며,
새로운 파일로 만들 수도 있다. 그 노드의 시작을 알리는 문자는 
@key{^_} 문자이며, 그 노드의 끝은 @key{^_}, @key{^L} 문자나, 
파일 끝 중 하나여야 한다. 주의: 만약 @key{^_} 문자로 
노드를 끝낸다면, 다음 노드의 시작을 알리는 @key{^_} 문자가 
있기 전까지의 내용을 그 파일 보기에서 보여지지 않는다. 
왜냐하면, 노드의 시작 문자는 @key{^_}이기 때문. 그래서, 
@key{^_}문자와 함께 그 노드를 끝내려면, 항상 @key{^_}문자를 바로 
다음에 표기해 주는 것이 좋다.

  노드의 시작을 알리는 @key{^_}문자는 받드시 그 줄의 
첫칸에 와야하거나, 아니면, @key{^L}문자 다음에 나와야 한다.
그 다음 그 노드의 머릿말이 온다. 그 노드의 머릿말에는 
최소한 그 노드의 이름이 있어야 한다. 그래야, info에서 
해당노드로 이동할 수 있다. 덧붙혀, 다음 노드@samp{Next}, 
이전 노드@samp{Previous}, 
상위 노드@samp{Up}의 이름을 대부분 지정하는 것이 보통이다. 
이 노드에서는 보는 것과 같이 @samp{Up} 노드는 @samp{Top}으로
되어 있으며, 이것은 이 info에 대한 info 파일의 처음으로 
이동하게 해 준다. 

  @dfn{Node}, @dfn{Previous}, @dfn{Up}, @dfn{Next} 키워드는 
그 순서가 임의로 정해 질 수 있다. 이렇게 되면 그 순서는 
정해진 되로 나타난다. 각각의 키워드의 부분은 ``키워드 (파일이름)노드이름, 
공백문자|탭문자'' 형식으로 되어야 한다.  
노드의 이름은 탭문자나 쉼표나 줄바꿈 문자로 끝나야한다. 
공백문자는 노드 이름에 포함된다. 왜냐하면, 공백문자가 있는 
노드도 있기 때문. 또한 노드의 이름에서 대소문자 구분이 있다.

  노드 이름을 지정하는 방법에는 두가지가 있다. 
그 이름은 @samp{Node: } 다음에 지정한다. 예를 들어, 이 노드의 이름은 
@samp{추가}라는 이름이다. 다른 파일 안에 있는 노드의 이름은 
@samp{(@var{파일이름})@var{노드이름}} 형식을 취한다. 이 노드의 경우는 
@samp{(info)추가} 이런 식이 된다. 파일 이름을 지정할 때, ``./'' 문자를
먼저 사용하게 되면, 현재 경로안에서 그 파일을 찾고 경로가 
지정되어 있지 않으면, info 파일이 있는 경로(INFOPATH 환경 변수값)에서 
찾는다. 
@samp{(@var{파일이름})Top} 이런 식의 노드 이름 지정은 단지 
@samp{(@var{파일이름})} 식으로 지정해도 된다. 관례에 따라, 
@samp{Top} 노드는 그 파일의 최상위 노드가 되며, 이 노드에서 
@samp{Up} 노드로의 이동은 @file{(dir)} 파일을 읽게 된다. 
@file{(dir)} 파일에서는 info 문서 시스템 전체의 최상위 노드가 
지정되어 있다. 그래서, 보통 @samp{Top} 노드의 상위 노드는 
@samp{Up: (dir)} 이런 식으로 @file{(dir)} 파일을 지정한다.

  노드 이름이 @kbd{*} 표인 경우는 특별한 의미로 사용된다. 
이것은 현재 파일의 전체 내용을 뜻한다. 그래서, @kbd{g*} 명령은 
현재 볼 수 있는 내용외의 옛날 내용이나, 현재 info 문서 구조에 
포함되어 있지 않는 내용들을 보여준다. 

  @samp{Node:} 다음에 지정되는 노드 이름은 그 info 파일의 이름과 
같아서도 안되며, 그 info 파일 전체를 통틀어 한번만 사용되어야 한다.
또한 이 노드 이름앞에 파일 이름을 지정해서도 안된다. 
이 이름은 이 노드 자체이기 때문. 
@samp{Next}, @samp{Previous}, @samp{Up} 키워드에서 사용할 노드 이름이
바로 이 이름이 되기 때문이다. 

  info를 보면 @samp{File:} 이라며, 파일 이름을 보여 주는 부분이 
머릿말에 나타난다. 이것은 실재 info 수행에 있어, 영향을 주지
않는다. 단지, 사용자가 현재 읽고 있는 노드가 어느 파일에 있는 
것인지를 알려주는 도움을 줄 뿐이다.

@node 차림표, 상호참조, 추가, 고급 Info
@comment  node-name,  next,  previous,  up
@section 차림표 만드는 방법

  Info 문서의 구조 안에서 어떠한 노드에도 그 노드에 대한 하위노드의 
목록인 차림표를 구성할 수 있다. 이것은 @kbd{m} 명령은 현재 노드에서
이 차림표의 목록을 찾아 그 목록 중에 있는 특정 노드로 이동하게 
해준다.

  차림표의 지정은 줄 첫칸에 @samp{* Menu:} 라는 문자를 지정함으로 
시작된다. 
나머지 부분은 차림표의 설명부분이 된다. 실질적인 각 차림표의 
항목은 그 줄의 첫칸에 @samp{* } 표를 시작함으로 지정된다.
각 항목의 이름 - 즉, @kbd{m} 명령을 사용할 때, 그 명령의 
인자가 되는 것 - 은 @samp{* } 표 다음 하나의 공백 문자가 있고, 
그다음에 지정한다. 다음에 콜론(:), 공백문자 또는 탭문자, 다음에, 
실질적으로 이동되는 그 노드의 이름이 온다. 이 노드 이름의 끝을 알리는 
문자는 탭문자, 쉼표, 줄바꿈 문자로 한다. 하지만, 대부분 마침표(.)로 
한다. 이 노드의 이름이 바로 @samp{Next}, @samp{Previous},
@samp{Up} 노드의 이름으로 사용되는 것이다. 

  한편, 노드의 이름과 항목의 이름이 같을 경우는 @samp{* 이름::}
이런 형식을 취한다. 주로 이런 방식을 취하는데, 이것은 
사용자로 하여금 노드이름과 항목 이름 사이의 혼돈을 막고, 
화면상 보다 깔끔해 보이기 때문이다.

  
  차림표를 만들 때는 몇가지 점들을 고려해야한다. 그 info 문서를 
읽는 사람들을 생각해서, 각 항목들의 이름을 너무 길거나, 
쉽게 눈에 띄지 않는 곳에 있거나, 아무런 설명 없이 단지 차림표 
항목만 있거나, 단어 완성기능(일반적으로 글쇠입력중 @kbd{Tab} 글쇠를
쳐서 나머지 부분을 완성해 주는 기능) 등을 전혀 고려 하지 않은 
차림표는 좋은 차림표가 못된다. 또한 부득이한 경우가 아니고는 
한 차림표 전체가 화면에 보여지도록 하는 것이 좋다. 
(보통 다섯 항목 이하가 적당하다)

  각 항목에 의해서 이동되어 진 노드는 차림표가 있는 노드에 대해서 
``하위 노드''가 된다. 그래서, 그 종족관계를 잘 따져 노드의 머릿말 
부분을 정의할 때, @samp{Up:} 부분에서 사용할 노드의 지정에 
신경을 써야한다. 가끔 그렇게 많이 info 문서의 구조가 복잡하지 않은 
경우는 이런 종속관계를 무시하고, @samp{Next}, @samp{Previous} 노드
지정 만으로, 문서 전체를 읽을 수 있도록 하기도 한다.

  Info 문서 시스템의 최상위 노드의 이름은 간단히 @samp{(dir)Top}으로 
정해져 있다. 이것은 @file{.../info/dir} 파일을 info로 읽는 역활을 한다.
이 파일에 위에서 설명한 방식대로 새로운 차림표 항목을 추가 할 수도 있다. 
또한 특정 경로안에 있는 info 파일을 지정할 수도 있으나, 
그렇게 하는 것보다, 그 info 파일을 info 파일이 있는 경로 안으로 
옮겨 놓는 것이 좋다. 

  또한 Info의 각 노드는 구조는 ``계층구조''라고 하지만, 
실은 어떠한 직통 연결도 가능하다.(한 하위노드에서 그 노드의 상위의 상위의
차림표 중에 있는 어떠한 노드와도 연결이 가능하다 - 옮긴이 말)
즉, 글의 구조와 글 분기점들의 순환등이 공유되어서, 이런 기능들이 
구현되었고, 만약 이런 기능들이 필요하다면, 사용되어 질 수 있다. 
하나의 노드가 하나의 문서 구조만을 가져야 할 필요는 없다. 
실제로, 지금 읽고 있는 이 파일에도 @samp{Top}에 의한 하나의 
문서 구조와 @samp{Help} 노드에 의한 또 다른 문서 구조가 
있다. 결국 문서 안의 어떠한 다른 문서 구조를 가지고 있는 
부분이 있다면, 그것을 발견하지 못할 경우에는 아무도 읽지 
못하는 상태를 만들어 내기도 한다. 
(이런 이야기는 M$-WINDWOS의 도움말 시스템과 비슷한 
점이 많음으로 그것을 참고하는 것도 좋을 듯 - 옮긴이 말)

@node 상호참조, 태그, 차림표, 고급 Info
@comment  node-name,  next,  previous,  up
@section 상호참조 만들기

  상호참조는 현재 노드의 어떤 위치에도 올 수 있다. 차림표는 
받드시 그 줄의 첫줄에 정의되어야한다. 상호참조는 @kbd{*} 이렇게 
차림표가 나타는 것과 달리 @samp{*note} 글자로 나타난다. 
상호참조는 @samp{)}문자로 끝날 수 @emph{없다}, @samp{)} 문자는 
가끔 노드 이름으로도 사용되기 때문이다. 굳이 상호참조를 괄호로 
묶겠다면, 괄호 앞에, 즉 상호참조의 설명이 끝나는 부분에 점(.)을 
찍는다. 아래에 두가지 상호참조 예제가 있다.

@example
@xref{장난, 예제}.  (See *Note 괄호묶은 예제: Full Proof.)
@end example

이것은 단지 예제일 뿐이니, 실제 상호 참조는 이루워 지지 않는다.

@node 장난,  ,  , 상호참조
@c 장난삼아 만들어 본것임.
@section 장난

  이 노드는 옮긴이가 장난 삼아 만들어 본것입니다. 
  ``이것은 단지 예제일 뿐이니, 실제 상호 참조는 이루워 지지 않는다''고 
말 했는데도 꼭 실행 시켜 보는 사람이 있더라구요. 껄껄.

  바로 @kbd{l} 쳐서 읽던 것 마저 읽으세요. 껄껄.
 
@node 태그, 검사하기, 상호참조, 고급 Info
@comment  node-name,  next,  previous,  up
@section Info 파일을 위한 Tags Tables 

  tags table이란 Info 파일의 크기가 클 경우 노드 이동의 속도를 
향상시키기 위해, 그 Info 파일 안에 있는 하나의 목록이다. 이것은 
그 파일이 읽혀질 때 자동으로 같이 읽혀진다. 

  이것은 그 info 파일 자체에서 편집한다는 것은 불가능하고, 
Emacs 편집기를 Info 모드로 열어서 @kbd{M-x Info-tagify} 이렇게 
입력해서 만든다. 그런 다음 반드시 @kbd{C-x C-s} 명령으로 바뀌
내용을 저장해야 한다.

  이 tag table은 info 파일의 내용이나, 그 구조가 바뀌었을 때, 
꼭 다시 만들어 주어야 한다. 옛날 tag table이 그대로 사용된다면, 
각 노드로 바르게 이동되지 않을 수도 있다. 

  tag table은 그 파일의 끝에 보통 다음과 같은 형식으로 타나난다.

@example
^_
Tag Table:
File: info, Node: Cross-refs^?21419
File: info,  Node: Tags^?22145
^_
End Tag Table
@end example

@noindent
위에서 보는 바와 같이 각 줄은 하나의 노드로 구성되어 있고, 
숫자들은 그 노드가 시작하는 위치를 뜻하며, 노드의 이름과 그 
숫자 사이에는 ^?(Delete) 문자가 있다.

@node 검사하기, Emacs Info 변수들, 태그, 고급 Info
@comment  node-name,  next,  previous,  up
@section Info 파일 검사하기

  Info 파일을 Texinfo 파일을 통해서 만들지 않고, 그냥 만든다면, 
쉽게 실수 하는 것이 바로 노드의 이름이다. 즉, 이전 노드, 
다음 노드를 이동하는 것에 대한 노드 이름의 지정이 잘 못되면, 
그것을 누가 발견하기 전까지는 아무도 모르는 상태로 남아있게 된다. 

  이런 경우를 방지하려면 가장 원시적인 방법으로 모든 노드에서 
이동 가능한 모든 노드로 직접 이동해 보는 방법이다. 즉, 각 차림표의 
항목들, 그리고, 각 상호참조들 차근히 체크해 보는 방법이 있다. 

  큰 문서일 경우에는 이런 방식의 확인 작업은 실로 엄청난 노력을 
요구한다. 이런 문제점들을 해결하기 위해서 emacs에서는 
Info-mode로 설정한 후 @kbd{M-x Info-validate} 입력함으로 
간단히 그 Info 파일을 검사할 수 있다. 

  (가장 좋은 방법은 역시 Texinfo 파일로 만드는 것이다 - 옮긴이 말)

@node Emacs Info 변수들, , 검사하기, 고급 Info
@section Emacs Info-mode 변수들

다음에 나열되는 변수들은 Emacs에서 Info-mode의 상태를 바꾸는 
역할을 한다. 필요에 따라 여러가지 변수들을 마음대로 그때 그때 
정할 수 있으며, 또한 초기화 파일인 @file{~/.emacs} 파일안에 
지정해 둘 수도 있다.
@xref{Examining,변수 지정과 검사, 
Examining and Setting Variables, emacs, GNU Emacs
설명서}.

@table @code
@item Info-enable-edit
@code{nil} 값으로 지정하면, @samp{e}(@code{Info-edit}) 명령을 
사용할 수 없게 한다. @code{nil} 그외의 값을 지정하면, 
Info 파일을 편집할 수 있다. @xref{추가, 편집}.

@item Info-enable-active-nodes
@code{nil} 아닌 값을 지정하면, 노드와 연결되어 있는 
Lisp 코드를 실행한다. 즉, 그 노드로 이동하게 되면, Lisp 코드가 
실행된다. 

@item Info-directory-list
Info 파일을 찾을 경로. 각 요소는 문자열(경로이름)이거나, 
아니면, @code{nil}(초기값)이다.

@item Info-directory
Info의 최상위 노드가 있는 파일이름 지정. 이 값은 
@code{Info-directory} 함수가 호출 될 때만 사용된다.
(GNU Emacs 19.34.2 버전(hanemacs)에서는 이 변수가 없네요 - 옮긴이 말)
@end table

@node Info 파일 만들기,  , 고급 Info, Top
@comment  node-name,  next,  previous,  up
@chapter Texinfo 파일에서 makeinfo를 이용한 Info 파일 만들기

@code{makeinfo} 풀그림은 Texinfo 파일에서 Info 파일을 만들어 
주는 연장틀이다. GNU Emacs에서는 @code{texinfo-format-region},
@code{texinfo-format-buffer} 함수가 같은 역활을 한다.

@xref{Create an Info File, , Creating an Info File, texinfo, the Texinfo
Manual}, 하나의 Texinfo 파일에서 Info 파일을 만드는 방법을 이야기 하고 있다.

@xref{Top,, Overview of Texinfo, texinfo, Texinfo: The GNU Documentation
Format}, Texinfo 파일을 만드는 방법을 이야기 하고 있다.

@nwnode info 풀그림 사용하기, 옵션들, , Top
@chapter Info 풀그림 사용하기
@lowersections
@c info-stnd.ko.texi 파일에서 이 이야기를 다루고 있다.
@paragraphindent 2
@include info-stnd-ko.texi
@raisesections
@bye







